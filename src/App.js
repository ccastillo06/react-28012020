import React, { useState } from "react";
import uuid from "uuid";

import { seedProfiles } from "./constants/seedProfiles";

import ProfileList from "./components/ProfileList";
import ProfileForm from "./components/ProfileForm";

import "./App.css";

const title = "Upgrade UI";
const initialState = {
  profiles: seedProfiles,
  editingProfile: null
};

function App() {
  const [state, setState] = useState(initialState);
  const { profiles, editingProfile } = state;

  function createProfile(profile) {
    const newProfile = {
      ...profile,
      id: uuid()
    };

    const newProfiles = [...profiles, newProfile];
    setState({ ...state, profiles: newProfiles });
  }

  function deleteProfile(id) {
    // Filter the profile with the given id.
    const newProfiles = profiles.filter(profile => profile.id !== id);
    // Set the state with the previous one and the new profiles array as profiles.
    setState({ ...state, profiles: newProfiles });
  }

  function setEditingProfile(id) {
    const profile = profiles.find(prof => prof.id === id);
    setState({ ...state, editingProfile: profile });
  }

  function editProfile(editedProfile) {
    const doesExist = profiles.findIndex(({ id }) => id === editedProfile.id);

    if (doesExist !== -1) {
      console.log('editing')
      const newProfiles = profiles.map(profile =>
        editedProfile.id === profile.id ? editedProfile : profile
      );

      setState({
        ...state,
        profiles: newProfiles,
        editingProfile: null
      });
    } else {
      createProfile(editedProfile);
    }
  }

  return (
    <div>
      <h1>{title}</h1>
      <ProfileForm
        onSubmitProfile={createProfile}
        editingProfile={editingProfile}
        onEditProfile={editProfile}
      />
      <ProfileList
        profiles={profiles}
        onClickDelete={deleteProfile}
        onClickEdit={setEditingProfile}
      />
    </div>
  );
}

export default App;
