import React from "react";
import PropTypes from "prop-types";

import ProfileCard from "../ProfileCard";

import "./styles.scss";

function ProfileList({ profiles, onClickEdit, onClickDelete }) {
  return (
    <div className="ProfileList__container">
      {profiles.map(student => (
        <ProfileCard
          key={student.id}
          profile={student}
          onClickEdit={onClickEdit}
          onClickDelete={onClickDelete}
        />
      ))}
      {profiles.length ? null : <h3>There are no profiles defined</h3>}
    </div>
  );
}

ProfileList.propTypes = {
  profiles: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      age: PropTypes.number,
      picture: PropTypes.string
    })
  ),
  onClickEdit: PropTypes.func,
  onClickDelete: PropTypes.func
};

ProfileList.defaultProps = {
  profiles: [],
  onClickEdit: null,
  onClickDelete: null
};

export default ProfileList;
