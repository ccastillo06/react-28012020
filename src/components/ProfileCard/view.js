import React from "react";
import PropTypes from "prop-types";

import "./styles.scss";

const defaultPicture =
  "https://portal.staralliance.com/imagelibrary/aux-pictures/prototype-images/avatar-default.png/@@images/image.png";

function ProfileCard({ profile, onClickEdit, onClickDelete }) {
  const { id, name, age, picture } = profile;

  function handleOnClickEdit() {
    onClickEdit(id);
  }

  function handleClickDelete() {
    onClickDelete(id);
  }

  return (
    <div className="ProfileCard__container">
      <img
        src={picture || defaultPicture}
        alt={`${name}'s avatar`}
        width="150"
      />
      <p>
        <b>Name:</b> {name}
      </p>
      <p>
        <b>Age:</b> {age}
      </p>
      <div>
        {onClickEdit ? (
          <button
            className="ProfileCard__button ProfileCard--edit"
            type="button"
            onClick={handleOnClickEdit}
          >
            Edit
          </button>
        ) : null}
        {onClickDelete ? (
          <button
            className="ProfileCard__button ProfileCard--danger"
            type="button"
            onClick={handleClickDelete}
          >
            Delete
          </button>
        ) : null}
      </div>
    </div>
  );
}

ProfileCard.propTypes = {
  profile: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    age: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    picture: PropTypes.string
  }).isRequired,
  onClickEdit: PropTypes.func,
  onClickDelete: PropTypes.func
};

ProfileCard.defaulProps = {
  onClickEdit: null,
  onClickDelete: null
};

export default ProfileCard;
