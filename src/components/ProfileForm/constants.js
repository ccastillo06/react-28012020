export const initialFormState = {
  name: '',
  age: '',
  picture: '',
}

export const formFields = [
  {
    id: 'name',
    label: 'Name',
    type: 'text',
  },
  {
    id: 'age',
    label: 'Age',
    type: 'number',
    min: 1,
  },
  {
    id: 'picture',
    label: 'Picture',
    type: 'text',
  },
]