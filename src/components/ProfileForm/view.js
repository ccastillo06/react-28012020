import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

import { formFields, initialFormState } from "./constants";
import { isProfileValid } from "./utils";

import ProfileCard from "../ProfileCard";

import "./styles.scss";

function ProfileForm({ editingProfile, onSubmitProfile, onEditProfile }) {
  const [formState, setFormState] = useState(initialFormState);

  useEffect(() => {
    if (editingProfile) {
      setFormState(editingProfile);
    }
  }, [editingProfile]);

  function handleSubmit(e) {
    e.preventDefault();

    if (editingProfile) {
      onEditProfile(formState);
    } else {
      onSubmitProfile(formState);
    }

    setFormState(initialFormState);
  }

  function handleOnChangeName(e) {
    const { value } = e.target;
    setFormState({ ...formState, name: value });
  }

  function handleOnChangeAge(e) {
    const { valueAsNumber } = e.target;
    const age = Number.isNaN(valueAsNumber) ? "" : valueAsNumber;
    setFormState({ ...formState, age });
  }

  function handleOnChangePicture(e) {
    const { value } = e.target;
    setFormState({ ...formState, picture: value });
  }

  const formFunctionMap = {
    name: handleOnChangeName,
    age: handleOnChangeAge,
    picture: handleOnChangePicture
  };

  return (
    <form className="ProfileForm__container" onSubmit={handleSubmit}>
      <fieldset>
        <legend>Create a new profile</legend>

        <div className="ProfileForm__content">
          <div className="ProfileForm__form-fields">
            {formFields.map(({ id, label, ...rest }) => (
              <label key={id} htmlFor={id}>
                <span>{label}</span>
                <input
                  id={id}
                  value={formState[id]}
                  onChange={formFunctionMap[id]}
                  {...rest}
                />
              </label>
            ))}
            <button
              disabled={!isProfileValid(formState.name, formState.age)}
              className="ProfileForm--submit"
              type="submit"
            >
              Submit
            </button>
          </div>

          <ProfileCard profile={formState} />
        </div>
      </fieldset>
    </form>
  );
}

ProfileForm.propTypes = {
  editingProfile: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    age: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    picture: PropTypes.string
  }),
  onSubmitProfile: PropTypes.func.isRequired,
  onEditProfile: PropTypes.func.isRequired
};

ProfileForm.defaultProps = {
  editingProfile: null
};

export default ProfileForm;
